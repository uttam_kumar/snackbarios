//
//  ViewController.swift
//  SnackBar
//
//  Created by Syncrhonous on 8/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialSnackbar
import MaterialComponents.MaterialSnackbar_ColorThemer
import TTGSnackbar

class ViewController: UIViewController {
    let message = MDCSnackbarMessage()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func showSnackbar(_ sender: UIButton) {
        let message = MDCSnackbarMessage()
        message.text = "ios snackbar"
        MDCSnackbarManager.alignment = .center
        MDCSnackbarManager.show(message)
    }
    
    @IBAction func showSnackbarAction(_ sender: UIButton) {
        //for material snackbar
        //materialSnackbar()
        
        //for third party TTGSnackbar
        ttgSnackbar()
    }
    
    
    @IBAction func dismissSnackbar(_ sender: UIButton) {
        snackbar.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        snackbar.messageTextAlign = .center
        snackbar.message = "online"
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            self.snackbar.dismiss()
        }
        //snackbar.dismiss()
    }
    
    let snackbar = TTGSnackbar(message: "TTGSnackbar !", duration: .forever)
    
    func ttgSnackbar(){
        snackbar.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        snackbar.messageTextAlign = .center
        snackbar.message = "Not Connected"
//        let snackbar = TTGSnackbar(message: "TTGSnackbar !",
//                                   duration: .forever,
//                                   actionText: "Action",
//                                   actionBlock: { (snackbar) in
//                                    print("Click action!")
//                                    // Dismiss manually after 3 seconds
//                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
//                                        snackbar.backgroundColor = #colorLiteral(red: 0.3068847047, green: 0.7658930421, blue: 0.2954145416, alpha: 1)
//                                        snackbar.dismiss()
//                                    }
//        })
    
        snackbar.show()
        
    }
    
    
    func materialSnackbar(){
        print("glgn")
        let colorScheme = MDCSemanticColorScheme()
        let message = MDCSnackbarMessage()
        
        let action = MDCSnackbarMessageAction()
        let actionHandler = {() in
            let answerMessage = MDCSnackbarMessage()
            answerMessage.text = "Fascinating"
            MDCSnackbarManager.show(answerMessage)
            
            
        }
        action.handler = actionHandler
        action.title = "OK"
        message.action = action
        
        message.text = "ios snackbar"
        MDCSnackbarManager.show(message)
        MDCSnackbarColorThemer.applySemanticColorScheme(colorScheme)
    }
}

